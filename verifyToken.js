const jwt = require('jsonwebtoken');
const localStorage = require("localStorage");
const User = require('./schema/User');
const encryption = require('bcrypt');

function authUserVerify(req,res,next) {
    const token = localStorage.getItem('token-id1');
    if(!token) return res.status(401).send('Not Authroised to Access the Dashboard');
    try {
        const verifyToken = jwt.verify(token,process.env.TOKEN_SECRET);
        req.user = verifyToken;
        next();
    }catch(err) {
        res.status(400).send('Something went Wrong');
    }
}

function deAuthUser(req,res,next) {
    const token = localStorage.getItem('token-id1');
    try {
        localStorage.removeItem('token-id1');
        next();
    }catch(err){
        req.status(400).send('Could not log out');
    }
}

async function displayUserDetails (req,res) {
    try{
        const token = localStorage.getItem('token-id1');
        const verifyToken = jwt.verify(token,process.env.TOKEN_SECRET);
        req.user = verifyToken;
        const details = await User.findById({
            _id : verifyToken._id
        })
        res.send(details);
        
    } catch(err) {
        res.send(err);
    }
    
}
async function deleteAccount(req,res) {
    try {
        const token = localStorage.getItem('token-id1');
        const verifyToken = jwt.verify(token,process.env.TOKEN_SECRET);
        req.user = verifyToken;
        const details = await User.findByIdAndDelete({
            _id : verifyToken._id
        })
        res.send('Account Deleted!');
        console.log('User deleted');
        localStorage.removeItem('token-id1');
    } catch(err) {
        res.send('Something went wrong');
    }
       

}

async function updateNameAndPassword(req,res,next) {
    try {
        const hash = encryption.hashSync(req.body.passord,10);
        const token = localStorage.getItem('token-id1');
        const verifyToken = jwt.verify(token,process.env.TOKEN_SECRET);
        req.user = verifyToken;
        const updateDetails = await User.findByIdAndUpdate({
            _id : verifyToken._id
        }, {
            navn: req.body.navn,
            passord: hash
        })

        res.send(updateDetails);
        console.log('Changing user details...');
        next();
    } catch(err) {
        res.send(err);
    }
}
module.exports.authUserVerify = authUserVerify;
module.exports.deAuthUser = deAuthUser;
module.exports.displayUserDetails = displayUserDetails;
module.exports.deleteAccount = deleteAccount;
module.exports.updateNameAndPassword = updateNameAndPassword;