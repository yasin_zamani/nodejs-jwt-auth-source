// Imports
const express = require('express');
const authRute = require('./rute/auth');
const mgoose = require('mongoose');
const enviormentdot = require('dotenv');
const path = require('path');
const bodyParser = require('body-parser');
const userDataRoute = require('./rute/userdata');
enviormentdot.config();
// main functionality

const app = express();
const port = process.env.PORT || 3000;




// connection to the database
mgoose.connect(process.env.MONGODB_CONNECT,    
    {useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true },
     ()=> {
    console.log('Connected to DataBase');
});
mgoose.set('useFindAndModify', false);
app.use(express.json());
app.use(bodyParser.urlencoded({extended:false}))
// when we do post request to /register or login. it will route it to /api/user/register
app.use('/api/user', authRute);
app.get('/api', (res,req) => {
    req.send('Welcome to API Authentication with JWT made by Yasin Zamani Konari')
})
app.use('/api/data', userDataRoute);
app.use('/api/data/logout', userDataRoute);
app.use('/api/data/details', userDataRoute);
app.use('/api/data/details/delete', userDataRoute);
app.use('/api/data/details/update', userDataRoute);
app.use('/', express.static(path.join(__dirname,'static')))
app.listen(port, () => console.log(`Server Running on port ${port}`  ));

