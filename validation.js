// Validation
const Joi = require('@hapi/joi');


// registration validaiton

const registrationValidation = data => {
    const schema = Joi.object({
        navn: Joi.string().min(6).required(),
        epost : Joi.string().min(6).email(),
        passord: Joi.string().min(6).required()
    })
    return schema.validate(data)
}
const loginValidation = (data) => {
    const schema = Joi.object({
        epost : Joi.string().min(6).email(),
        passord: Joi.string().min(6).required()
    })
    return schema.validate(data)
}


module.exports.registrationValidation = registrationValidation;
module.exports.loginValidation = loginValidation;
