// route seperation 
// do not want to overfill index.js file.

const router = require('express').Router();
const encryption = require('bcrypt');
const User = require('../schema/User');
const {registrationValidation, loginValidation} = require('../validation');
const jwt = require('jsonwebtoken');
const localStorage = require("localStorage");
// sending the post data
const registerDirectory = '/register';
const loginDirectory =  '/login';


router.post(registerDirectory, async (req,res)=> {
    // response test with The Postman app
    //  res.send('Sending Register Response')
    const hashedpassword = encryption.hashSync(req.body.passord,10);

   const {error} =  registrationValidation(req.body)
   if(error) return res.status(400).send(error.details[0].message);
   const emailExists = await User.findOne({epost: req.body.epost});
   if(emailExists) return res.status(400).send('User Already Registered');
    const bruker = new User({
        navn: req.body.navn,
        epost: req.body.epost,
        passord: hashedpassword
    })
    try{
        const savedUser = await bruker.save()
        res.send(savedUser);
        console.log('Creating new User');
    }catch(error) {
        res.status(400).send(error);
    }
    res.end();
})

router.post(loginDirectory, async (req,res) => {
    const {error} =  loginValidation(req.body)
    if(error) return res.status(400).send(error.details[0].message);
    const emailExists = await User.findOne({epost: req.body.epost});
    if(!emailExists) return res.status(400).send('Details are incorrect!');
    const validPass = await encryption.compare(req.body.passord, emailExists.passord);
    if(!validPass) {
        return res.status(400).send('Not a valid password');
    }
    const tokenSession = jwt.sign({_id: emailExists._id}, process.env.TOKEN_SECRET, {expiresIn : '60s'})
    res.header('token-id', tokenSession)
    res.send(tokenSession);
    localStorage.setItem('token-id1', tokenSession) 
    console.log(localStorage.getItem('token-id1'))
})


router.get('/login/:id', async (req,res)=> {
    const getinfo = await User.findById({
        _id : req.params.id
    })

    if(getinfo) {
        return res.status(200).send(getinfo.epost) 
    } else {
        return res.status(400).send('No User Found') 
    }
})




module.exports = router;