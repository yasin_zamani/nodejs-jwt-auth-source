const router = require('express').Router();
const {authUserVerify, deAuthUser, displayUserDetails, deleteAccount, updateNameAndPassword} = require('../verifyToken');
router.get('/', authUserVerify, (req,res) => {
    res.sendFile(__dirname + '/dashboard.html');
})

router.get('/logout', deAuthUser,(req,res) => {
    res.send('Logged out');
})

router.get('/details', displayUserDetails, (res,req) =>{
    req.send(req);
})

router.get('/delete',deleteAccount,(res,req) => {
    req.send('Account Deleted');
})

router.post('/update',updateNameAndPassword,(res,req) => {
    
})
module.exports = router;