const mgoose = require('mongoose');

// Create user Schema 

// dette har jeg lært om, NoSQL, pass på bcrypt hashing (255). 
// Ikke gjør samme feil med password length i SQL.


const userSchema = new mgoose.Schema({
    navn: {
        type: String,
        required: true,
        min: 6
    },
    epost: {
        type: String,
        required: true,
        min: 5,
        max: 255
    },

    passord: {
        type: String,
        required: true,
        min: 7,
        max : 255,
    },
})

module.exports = mgoose.model('User',userSchema);